package it.evonove.seam.i18n;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import org.jboss.seam.annotations.In;

@ManagedBean(name = "language")
@SessionScoped
public final class LanguageBean implements Serializable {
	@In
	private FacesContext facesContext;
	
	private static final long serialVersionUID = 1L;

	private String localeCode;

	private static Map<String, Object> countries;
	
	private static Locale locale;
	
	static {
		countries = new LinkedHashMap<String, Object>();
		countries.put("English", Locale.ENGLISH); // label, value
		countries.put("Italiano", Locale.ITALIAN);
	}
	
	public Locale getLocale(){
		if (locale == null){
			try{
				locale = facesContext.getViewRoot().getLocale();
			} catch (Exception ex){
				locale = Locale.ENGLISH;
			}
		}
		return locale;
	}
	public void setLocale(Locale newLocale){
		locale = newLocale;
	}
	
	public Map<String, Object> getCountriesInMap() {
		return countries;
	}
	
	
	//to manage changelanguage.xthml
	public String getLocaleCode() {
		return localeCode;
	}

	public void setLocaleCode(String localeCode) {
		this.localeCode = localeCode;
	}

	// value change event listener
	public void countryLocaleCodeChanged(ValueChangeEvent e) {

		String newLocaleValue = e.getNewValue().toString();

		// loop country map to compare the locale code
		for (Map.Entry<String, Object> entry : countries.entrySet()) {

			if (entry.getValue().toString().equals(newLocaleValue)) {
				locale = (Locale) entry.getValue();
				try {
					facesContext.getViewRoot().setLocale(locale);
				} catch (NullPointerException ex) {
					FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
				}

			}
		}
	}
}