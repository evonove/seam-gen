<#include "../util/TypeInfo.ftl">
<#assign entityName = pojo.shortName>
<#assign entityClass = pojo.qualifiedDeclarationName>
<#assign componentName = entityName?uncap_first>
<#assign listName = componentName + "List">
<#assign modelPackage=entityClass?substring(0,entityClass?last_index_of("."))>
<#assign restPackage=modelPackage?substring(0,modelPackage?last_index_of("."))+".rest">
<#assign hasOrphanRemoval="it.evonove.seam.ftl.FieldAnnotationOrphanRemoval"?new()>
package ${restPackage};

import ${entityClass};
import it.evonove.seam.rest.PersistentObjectSerializer;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.Form;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.graph.GraphAdapterBuilder;

@Name("${componentName}API")
@Path("/${componentName?lower_case}")
public class ${entityName}API {
	@In
	private EntityManager entityManager;
	
	@GET
	@Produces("text/json")
	@SuppressWarnings("unchecked")
	public String get() {
		GsonBuilder gsonBuilder = new GsonBuilder()<#foreach property in pojo.allPropertiesIterator><#if c2h.isOneToManyCollection(property) | isToOne(property)>
		<#if c2h.isOneToManyCollection(property)><#assign childPojo = c2j.getPOJOClass(property.value.element.associatedClass)>
			.registerTypeAdapter(${childPojo.packageName}.${childPojo.shortName}.class, new PersistentObjectSerializer<${childPojo.packageName}.${childPojo.shortName}>())
		<#else>
			.registerTypeAdapter(${property.value.referencedEntityName}.class, new PersistentObjectSerializer<${property.value.referencedEntityName}>())</#if></#if></#foreach>
			.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
		return gson.toJson((List<${entityName}>)entityManager.createQuery("from ${entityClass}").getResultList());
	}

	@GET
	@Path("/{${entityName}Id}")
	@Produces("text/json")
	public String get${entityName}Id(@PathParam("${entityName}Id") int id) {
		GsonBuilder gsonBuilder = new GsonBuilder()<#foreach property in pojo.allPropertiesIterator><#if c2h.isOneToManyCollection(property) | isToOne(property)>
		<#if c2h.isOneToManyCollection(property)><#assign childPojo = c2j.getPOJOClass(property.value.element.associatedClass)>
			.registerTypeAdapter(${childPojo.packageName}.${childPojo.shortName}.class, new PersistentObjectSerializer<${childPojo.packageName}.${childPojo.shortName}>())
		<#else>
			.registerTypeAdapter(${property.value.referencedEntityName}.class, new PersistentObjectSerializer<${property.value.referencedEntityName}>())</#if></#if></#foreach>
			.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
		return gson.toJson(entityManager.find(${entityName}.class, id));
	}

	@POST
	public Response post(@Form ${entityName}FormSupport form) {
		try {
			${entityName} ${entityName} = copyFields(form, new ${entityName}());
			entityManager.persist(${entityName});
			GsonBuilder gsonBuilder = new GsonBuilder();
			new GraphAdapterBuilder().addType(${entityName}.class).registerOn(gsonBuilder);
        	Gson gson = gsonBuilder.create();
			return Response.ok(gson.toJson(${entityName})).status(201).build();
		} catch (Exception ex) {
			return Response.serverError().build();
		}
	}
	
	@PUT
	@Path("/{${entityName}Id}")
	public Response put(@Form ${entityName}FormSupport form, @PathParam("${entityName}Id") int id) {
		try {
			GsonBuilder gsonBuilder = new GsonBuilder();
			new GraphAdapterBuilder().addType(${entityName}.class).registerOn(gsonBuilder);
        	Gson gson = gsonBuilder.create();
			${entityName} ${entityName} = entityManager.find(${entityName}.class, id);
			${entityName} = copyFields(form, ${entityName});
			entityManager.persist(${entityName});
			return Response.ok(gson.toJson(${entityName})).status(200).build();
		} catch (IllegalArgumentException ex) {
			return Response.status(400).build();
		} catch (Exception ex) {
			return Response.serverError().build();
		}
	}
	
	@DELETE
	@Path("/{${entityName}Id}")
	public Response delete(@PathParam("${entityName}Id") int id) {
		try {
			entityManager.remove(entityManager.find(${entityName}.class, id));
			return Response.ok(204).build();
		} catch (IllegalArgumentException ex) {
			return Response.status(400).build();
		} catch (Exception ex) {
			return Response.serverError().build();
		}
	}
	
	public static class ${entityName}FormSupport {
		<#assign String = "String"><#assign Integer = "Integer"><#assign Set = "java.util.Set"><#assign List = "java.util.List"><#foreach property in pojo.allPropertiesIterator><#assign type = property.type.name>
            <#if property.type.entityType>@FormParam("${property.name}")
            Integer ${property.name};</#if><#if type?cap_first?contains(String)>@FormParam("${property.name}")
            String ${property.name};</#if><#if type?cap_first?contains(Integer)>@FormParam("${property.name}")
            Integer ${property.name};</#if><#if property.type.collectionType><#if type?contains(Set)>@FormParam("${property.name}")
            Set<Integer> ${property.name};<#elseif type?contains(List)>@FormParam("${property.name}")
            List<Integer> ${property.name};<#else>@FormParam("${property.name}")
            Collection<Integer> ${property.name};</#if></#if>
        </#foreach>
	
		//TODO Collection support
		//TODO remove set operation on final field
		
		public ${entityName}FormSupport(){
		}
	}
	
	public <F,T> T copyFields(F source, T target){
	    Class<?> clazz = source.getClass();
	    for (Field field : clazz.getFields()) {
			try {
				Object value = field.get(source);
				Field targetField = target.getClass().getField(field.getName());
				targetField.setAccessible(true);
				targetField.set(target, value);
			} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    return target;
	}
}
