<#include "../util/TypeInfo.ftl">
<#assign entityName = pojo.shortName>
<#assign componentName = entityName?uncap_first>
<#assign listName = componentName + "List">
<#assign chainer="it.evonove.seam.ftl.FieldAnnotationChainer"?new()>
package ${actionPackage};

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

<#if pojo.packageName != "">
import ${pojo.packageName}.${entityName};
</#if>
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.web.RequestParameter;

@Name("${componentName}Action")
public class ${entityName}Action
{
    @In
    private EntityManager entityManager;
    private ArrayList<String> tabs = new ArrayList<String>(Arrays.asList(
        <#foreach property in pojo.allPropertiesIterator>
            <#if c2h.isOneToManyCollection(property)>
            "${property.name}",
            </#if>
        </#foreach>
            "details"
        ));

    public boolean tabManager(String tab) {
        return !tabs.contains(tab);
    }

    <#foreach property in pojo.allPropertiesIterator>
    	<#assign chain=chainer(property)>
        <#if chain.type = 'SLAVE' || chain.type = 'BOTH' >
	@SuppressWarnings("unchecked")
	public List<${chain.yourClassName}> get${chain.yourClassSimpleName?cap_first}Chained(${chain.masterClassName} ${chain.masterFieldName}) {
		return entityManager.createQuery("select ${chain.yourClassSimpleName?lower_case} from ${chain.yourClassName} ${chain.yourClassSimpleName?lower_case} " +
				"where ${chain.yourClassSimpleName?lower_case}.${chain.masterFieldName} = :${chain.masterFieldName}"
				).setParameter("${chain.masterFieldName}", ${chain.masterFieldName})
				.getResultList();
	}
                      
		</#if>	
    </#foreach>
}
