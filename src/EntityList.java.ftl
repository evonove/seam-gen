<#include "../util/TypeInfo.ftl">
<#assign entityName = pojo.shortName>
<#assign componentName = entityName?uncap_first>
<#assign listName = componentName + "List">
package ${actionPackage};

<#if pojo.packageName != "">
import ${pojo.packageName}.${entityName};
</#if>
import org.jboss.seam.annotations.Name;
import org.jboss.seam.framework.EntityQuery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.persistence.NoResultException;

@Name("${listName}")
public class ${entityName}List extends EntityQuery<${entityName}>
{

    private static final String EJBQL = "select ${componentName} from ${entityName} ${componentName}";

    private static final String[] RESTRICTIONS = {
<#foreach property in pojo.allPropertiesIterator>
<#if !c2h.isCollection(property) && !isToOne(property)>
<#if c2j.isComponent(property)>
<#foreach componentProperty in property.value.propertyIterator>
<#if isString(componentProperty)>
        "lower(${componentName}.${property.name}.${componentProperty.name}) like lower(concat(${'#'}{${listName}.${componentName}.${property.name}.${componentProperty.name}},'%'))",
</#if>
</#foreach>
<#else>
<#if isString(property)>
        "lower(${componentName}.${property.name}) like lower(concat(${'#'}{${listName}.${componentName}.${property.name}},'%'))",
</#if>
</#if>
</#if>
</#foreach>
    };

<#if pojo.isComponent(pojo.identifierProperty)>
    private ${entityName} ${componentName};
<#else>
    private ${entityName} ${componentName} = new ${entityName}();
</#if>
    private List<Integer> pageList = new ArrayList<Integer>();
    private int activePage = 0;
    private Boolean allResult = false;
    private HtmlDataTable htmlDataTable;
	
	public HtmlDataTable getHtmlDataTable() {
		return htmlDataTable;
	}

	public void setHtmlDataTable(HtmlDataTable htmlDataTable) {
		this.htmlDataTable = htmlDataTable;
	}

    public ${entityName}List()
    {
<#if pojo.isComponent(pojo.identifierProperty)>
        ${componentName} = new ${entityName}();
        ${componentName}.setId( new ${entityName}Id() );
</#if>
        setEjbql(EJBQL);
        setRestrictionExpressionStrings(Arrays.asList(RESTRICTIONS));
        if (allResult == true) {
            setMaxResults(null);
        } else {
            setMaxResults(25);
        }
    }

    public ${entityName} get${entityName}() {
        return ${componentName};
    }

    public List<Integer> getPageList() {
        for (int i = 1; i <= getPageCount(); i++) {
            pageList.add(i);
        }
        return pageList;
    }

    public void setPageList(List<Integer> pageList) {
        this.pageList = pageList;
    }

    public int getActivePage() {
        return activePage;
    }

    public void setActivePage(int activePage) {
        if (activePage >= 0 && activePage < getPageCount()) {
            this.activePage = activePage;
            setFirstResult(this.activePage * getMaxResults());
        }
    }

    public Boolean getAllResult() {
        return allResult;
    }

    public void setAllResult(Boolean allResult) {
        this.allResult = allResult;
        if (allResult == true) {
            setMaxResults(null);
        }
    }
    
    
    public void delete() {
		try {
			getEntityManager().remove((${entityName})htmlDataTable.getRowData());
			refresh();
		} catch (NoResultException e) {
			e.printStackTrace();
		}
	}
}
