<#include "../util/TypeInfo.ftl">
package ${actionPackage};
<#assign classbody>
<#assign entityName = pojo.shortName>
<#assign componentName = entityName?uncap_first>
<#assign homeName = componentName + "Home">
<#assign hasMethod="it.evonove.seam.ftl.HasMethod"?new()>
<#assign hasOrphanRemoval="it.evonove.seam.ftl.FieldAnnotationOrphanRemoval"?new()>
<#assign chainer="it.evonove.seam.ftl.FieldAnnotationChainer"?new()>


		

import java.util.Collection;
import java.util.HashSet;
import org.jboss.seam.annotations.Transactional;

import it.evonove.seam.PersistentObject;
import it.evonove.seam.home.Helper;
<#global chained = false>
<#foreach property in pojo.allPropertiesIterator>
	<#if !chained>
    	<#assign chain=chainer(property)>
    	<#if chain.connected>
    		<#global chained = true>
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.ScopeType;
    		
@Scope(ScopeType.PAGE)
    	</#if>
    </#if>
</#foreach>
@${pojo.importType("org.jboss.seam.annotations.Name")}("${homeName}")
public class ${entityName}Home extends ${pojo.importType("org.jboss.seam.framework.EntityHome")}<${entityName}>
{

<#assign parentHomeNames = []>
<#foreach property in pojo.allPropertiesIterator>
<#if isToOne(property)>
<#assign parentPojo = c2j.getPOJOClass(cfg.getClassMapping(property.value.referencedEntityName))>
<#assign parentHomeName = parentPojo.shortName?uncap_first + "Home">
<#if !parentHomeNames?seq_contains(parentHomeName)><#-- This doesn't fix the functionality, just allows compilation to work -->
<#assign parentHomeNames = parentHomeNames + [parentHomeName]>
    @${pojo.importType("org.jboss.seam.annotations.In")}(create=true)
    <#if parentPojo.packageName!="">${pojo.importType("${parentPojo.packageName}.${parentPojo.shortName}")}<#else>${parentPojo.shortName}</#if>Home ${parentHomeName};
</#if>
</#if>
</#foreach>

<#assign idName = entityName + pojo.identifierProperty.name?cap_first>
<#if c2j.isComponent(pojo.identifierProperty)>
<#assign idType = entityName + "Id">
<#else>
<#assign idType = pojo.importType(pojo.identifierProperty.type.returnedClass.name)>
</#if>
    public void set${idName}(${idType} id)
    {
        setId(id);
    }

    public ${idType} get${idName}()
    {
        return (${idType}) getId();
    }

<#if pojo.isComponent(pojo.identifierProperty)>
    public ${entityName}Home()
    {
        set${idName}( new ${entityName}Id() );
    }

    @Override
    public boolean isIdDefined()
    {
<#foreach property in pojo.identifierProperty.value.propertyIterator>
<#assign getter = pojo.getGetterSignature(property)>
<#if property.value.typeName == "string" || property.value.typeName == "java.lang.String" >
        if ( get${idName}().${getter}()==null || "".equals( get${idName}().${getter}() ) ) return false;
<#elseif !c2j.isPrimitive( pojo.getJavaTypeName(property, true) )>
        if ( get${idName}().${getter}()==null ) return false;
<#else>
        if ( get${idName}().${getter}()==0 ) return false;
</#if>
</#foreach>
        return true;
    }

</#if>
    @Override
    protected ${entityName} createInstance()
    {
        ${entityName} ${componentName} = new ${entityName}();
<#if pojo.isComponent(pojo.identifierProperty)>
        ${componentName}.setId( new ${entityName}Id() );
</#if>
        return ${componentName};
    }

    public void load()
    {
        if (isIdDefined())
        {
            wire();
        }
    }

    public void wire()
    {
        getInstance();
<#foreach property in pojo.allPropertiesIterator>
<#if isToOne(property)>
<#assign parentPojo = c2j.getPOJOClass(cfg.getClassMapping(property.value.referencedEntityName))>
<#if parentPojo.shortName!=pojo.shortName>
<#assign parentHomeName = parentPojo.shortName?uncap_first + "Home">
<#assign setter = "set" + pojo.getPropertyName(property)>
        ${parentPojo.shortName} ${property.name}=${parentHomeName}.getDefinedInstance();
        if ( ${property.name}!=null )
        {
           getInstance().${setter}(${property.name});
        }
</#if>
</#if>
</#foreach>
    }

    public boolean isWired()
    {
<#foreach property in pojo.allPropertiesIterator>
<#if (isToOne(property) && !property.optional)>
<#assign getter = pojo.getGetterSignature(property)>
        if ( getInstance().${getter}()==null ) return false;
</#if>
</#foreach>
        return true;
    }

    public ${entityName} getDefinedInstance()
    {
        return isIdDefined() ? getInstance() : null;
    }

<#foreach property in pojo.allPropertiesIterator>
	<#assign getter = pojo.getGetterSignature(property)>
	<#if c2h.isOneToManyCollection(property)>
		<#assign childPojo = c2j.getPOJOClass(property.value.element.associatedClass)>
		<#assign childPojoClassName><#if childPojo.packageName!="">${pojo.importType("${childPojo.packageName}.${childPojo.shortName}")}<#else>${childPojo.shortName}</#if></#assign>
		<#assign oneToManyBidirectional = !hasOrphanRemoval(property)>
		<#if !oneToManyBidirectional>
	${pojo.importType("java.util.List")}<${childPojoClassName}> raw${childPojoClassName}List = new ArrayList<${childPojoClassName}>(0);

	public void setRaw${childPojoClassName}List(List<${childPojoClassName}> raw${childPojoClassName}List){
		this.raw${childPojoClassName}List = raw${childPojoClassName}List;
	}

	public ${pojo.importType("java.util.List")}<${childPojoClassName}> getRaw${childPojoClassName}List() {
        return ${getter}();
    }
		</#if>

	public ${pojo.importType("java.util.List")}<${childPojoClassName}> ${getter}() {
        return getInstance() == null ?
            null : new ${pojo.importType("java.util.ArrayList")}<${childPojo.shortName}>( getInstance().${getter}() );
    }
	</#if>
</#foreach>	


<#assign writeOverride = true>	
<#assign colseOverride = false>	
<#foreach property in pojo.allPropertiesIterator>
	<#assign getter = pojo.getGetterSignature(property)>
	<#if c2h.isOneToManyCollection(property)>
		<#assign childPojo = c2j.getPOJOClass(property.value.element.associatedClass)>
		<#assign oneToManyBidirectional = !hasOrphanRemoval(property)>
		<#if writeOverride>
	@Override
	@Transactional
	public String update() {
		Helper helper = new Helper();
		PersistentObject po = (PersistentObject)getInstance();
			<#assign writeOverride = false>
			<#assign colseOverride = true>
		</#if>
		<#assign childPojoClassName><#if childPojo.packageName!="">${pojo.importType("${childPojo.packageName}.${childPojo.shortName}")}<#else>${childPojo.shortName}</#if></#assign>
		<#assign prefix><#if oneToManyBidirectional>Bi<#else>Mo</#if></#assign>
		<#assign collection><#if oneToManyBidirectional>${getter}()<#else>this.raw${childPojoClassName}List</#if></#assign>
		helper.oneToMany${prefix}Update(po, ${collection}, getEntityManager(), ${childPojoClassName}.class);
	</#if>
</#foreach>	
<#if colseOverride>
		return super.update();
	}
</#if>

<#assign writeOverride = true>	
<#assign colseOverride = false>	
<#foreach property in pojo.allPropertiesIterator>
	<#assign getter = pojo.getGetterSignature(property)>
	<#if c2h.isOneToManyCollection(property)>
		<#assign childPojo = c2j.getPOJOClass(property.value.element.associatedClass)>
		<#assign oneToManyBidirectional = !hasOrphanRemoval(property)>
		<#if writeOverride>
	@Override
	@Transactional
	public String persist() {
		Helper helper = new Helper();
		PersistentObject po = (PersistentObject)getInstance();
			<#assign writeOverride = false>
			<#assign colseOverride = true>
		</#if>
		<#assign childPojoClassName><#if childPojo.packageName!="">${pojo.importType("${childPojo.packageName}.${childPojo.shortName}")}<#else>${childPojo.shortName}</#if></#assign>
		<#assign oneToManyBidirectional = !hasOrphanRemoval(property)>
		<#assign prefix><#if oneToManyBidirectional>Bi<#else>Mo</#if></#assign>
		<#assign collection><#if oneToManyBidirectional>${getter}()<#else>this.raw${childPojoClassName}List</#if></#assign>
		helper.oneToMany${prefix}Persist(po, ${collection}, ${childPojoClassName}.class);
	</#if>
</#foreach>	
<#if colseOverride>
		return super.persist();
	}
</#if>
}
</#assign>

<#if pojo.packageName != "">
import ${pojo.packageName}.*;<#-- This import is necessary because we're using a different package than Hibernate Tools expects -->
</#if>
${pojo.generateImports()}
${classbody}
