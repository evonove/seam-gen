<!DOCTYPE composition PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<#include "../util/TypeInfo.ftl">
<#assign entityName = pojo.shortName>
<#assign componentName = entityName?uncap_first>
<#assign homeName = componentName + "Home">
<#assign actionName = componentName + "Action">
<#assign masterPageName = entityName + "List">
<#assign editPageName = entityName + "Edit">
<#assign hasOrphanRemoval="it.evonove.seam.ftl.FieldAnnotationOrphanRemoval"?new()>
<ui:composition xmlns="http://www.w3.org/1999/xhtml"
    xmlns:s="http://jboss.org/schema/seam/taglib"
    xmlns:ui="http://java.sun.com/jsf/facelets"
    xmlns:f="http://java.sun.com/jsf/core"
    xmlns:h="http://java.sun.com/jsf/html"
    xmlns:rich="http://richfaces.org/rich"
    template="/layout/template.xhtml">

<ui:define name="body">
    <div class="row-fluid">
        <div class="span12">
            <div class="well">
                <h2>${'#'}{msg['${label(entityName)}']} "${'#'}{${homeName}.instance}"</h2>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="well well-small form-inline form-no-margin">
                    <s:button id="edit" styleClass="btn btn-success" view="/admin/${editPageName}.xhtml" value="${'#'}{msg['Edit']}">
                        <f:param name="openTab" value="${'#'}{openTab}"/>
                    </s:button> &nbsp;
                    <s:button id="back" styleClass="btn" 
                    view="/admin/${'#'}{empty ${componentName}From or ${componentName}From == '${entityName}' ? '${masterPageName}' : ${componentName}From}.xhtml" 
                    value="${'#'}{msg['Back']}">
                        <f:param name="openTab" value="${'#'}{openTab}"/>
                    </s:button>
                </div>
            </div>
        </div>
        <ul class="nav nav-pills">
            <li class="${'#'}{empty openTab or ${actionName}.tabManager(openTab) ? 'active' : ''}"><a href="#generic-data" data-toggle="tab">${'#'}{msg['${label(entityName)}']} ${'#'}{msg['details']}</a></li>
            <#foreach property in pojo.allPropertiesIterator>
            <#if c2h.isCollection(property) && !c2h.isManyToMany(property)>
            <li class="${'#'}{not empty openTab and openTab == '${property.name}' ? 'active' : ''}"><a href="${'#'}${property.name}" data-toggle="tab">${label(property.name)}</a></li>
            </#if>
            </#foreach>
        </ul>
        <div class="tab-content">
            <div class="tab-pane ${'#'}{empty openTab or ${actionName}.tabManager(openTab) ? 'active' : ''}" id="generic-data">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="well">
                            <div class="indent">
                                <dl class="dl-horizontal">
                                    <#foreach property in pojo.allPropertiesIterator>
                                        <#if !c2h.isCollection(property) && property != pojo.versionProperty!>
                                            <#include "viewproperty.xhtml.ftl">
                                        </#if>
                                    </#foreach>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<#foreach property in pojo.allPropertiesIterator>
    <#if c2h.isOneToManyCollection(property)>
        <div class="tab-pane ${'#'}{not empty openTab and openTab == '${property.name}' ? 'active' : ''}" id="${property.name}">
            <div class="row-fluid">
                <div class="span12">
                    <div class="well">
	    <#assign childPojo = c2j.getPOJOClass(property.value.element.associatedClass)>
	    <#assign childPageName = childPojo.shortName>
	    <#assign childEditPageName = childPojo.shortName + "Edit">
	    <#assign childName = childPojo.shortName?uncap_first>
	    <#assign childHomeName = childName + "Home">
	    <#assign childAction = childName + "Action">
            <h:outputText value="There are no ${property.name} associated with this ${componentName}." rendered="${'#'}{empty ${homeName}.${property.name}}"/>
            <h:dataTable value="${'#'}{${homeName}.${property.name}}" var="_${childName}" styleClass="table table-hover" columnClasses="" rendered="${'#'}{not empty ${homeName}.${property.name}}">
	    <#assign hasOrphanRemoval = hasOrphanRemoval(property)>
	  	<#if !hasOrphanRemoval>
                <h:column>
                    <s:link styleClass="btn btn-success" view="/admin/${childEditPageName}.xhtml" propagation="none">
		    <#if childPojo.isComponent(childPojo.identifierProperty)>
		        <#foreach componentProperty in childPojo.identifierProperty.value.propertyIterator>
                            <f:param name="${childName}${componentProperty.name?cap_first}"
                                    value="${'#'}{_${childName}.${childPojo.identifierProperty.name}.${componentProperty.name}}"/>
	        	</#foreach>
	    	<#else>
                        <f:param name="${childName}${childPojo.identifierProperty.name?cap_first}"
                                value="${'#'}{_${childName}.${childPojo.identifierProperty.name}}"/>
    		</#if>
                        <f:param name="${childName}From" value="${entityName}"/>
                        <f:param name="openTab" value="${property.name}"/>
                        <i class="icon-pencil icon-white" /> Edit
                    </s:link>
                </h:column>
		</#if>
	    <#foreach childProperty in childPojo.allPropertiesIterator>
	        <#if !c2h.isCollection(childProperty) && !isToOne(childProperty) && childProperty != childPojo.versionProperty!>
	        <#if childPojo.isComponent(childProperty)>
	        <#foreach componentProperty in childProperty.value.propertyIterator>
                            <h:column>
                                <f:facet name="header">${'#'}{msg['${label(componentProperty.name)}']}</f:facet>
                                <@outputValue property=childProperty expression="${'#'}{_${childName}.${childProperty.name}}" indent=12/>
                            </h:column>
	        </#foreach>
	        <#else>
                            <h:column>
                                <f:facet name="header">${'#'}{msg['${label(childProperty.name)}']}</f:facet>
                                <@outputValue property=childProperty expression="${'#'}{_${childName}.${childProperty.name}}" indent=12/>
                            </h:column>
	        </#if>
	        </#if>
	    </#foreach>
	    <#if !hasOrphanRemoval>
                <h:column>
                    <s:link styleClass="btn btn-info" view="/admin/${childPageName}.xhtml">
		    <#if childPojo.isComponent(childPojo.identifierProperty)>
				<#foreach componentProperty in childPojo.identifierProperty.value.propertyIterator>
                            <f:param name="${childName}${componentProperty.name?cap_first}"
                                    value="${'#'}{_${childName}.${childPojo.identifierProperty.name}.${componentProperty.name}}"/>
				</#foreach>
			<#else>
                        <f:param name="${childName}${childPojo.identifierProperty.name?cap_first}"
                                value="${'#'}{_${childName}.${childPojo.identifierProperty.name}}"/>
			</#if>
                        <f:param name="${childName}From" value="${entityName}"/>
                        <f:param name="openTab" value="${property.name}"/>
                        <i class="icon-eye-open icon-white" /> ${'#'}{msg['Open']}
                    </s:link>
                </h:column>
                <h:column>
                    <s:link styleClass="btn btn-danger btn-confirm" propagation="none" action="${'#'}{${childAction}.delete}">
		    <#if childPojo.isComponent(childPojo.identifierProperty)>
				<#foreach componentProperty in childPojo.identifierProperty.value.propertyIterator>
                            <f:param name="${childName}${componentProperty.name?cap_first}"
                                value="${'#'}{_${childName}.${childPojo.identifierProperty.name}.${componentProperty.name}}"/>
		        </#foreach>
		    <#else>
                        <f:param name="${childName}${childPojo.identifierProperty.name?cap_first}"
                                value="${'#'}{_${childName}.${childPojo.identifierProperty.name}}"/>
			</#if>
                        <f:param name="${childName}From" value="${entityName}"/>
                        <i class="icon-trash icon-white" /> ${'#'}{msg['Delete']}
                    </s:link>
                </h:column>
		</#if>
            </h:dataTable>
        <#if !hasOrphanRemoval>
                    <div class="actionButtons">
                        <s:button
                          styleClass="btn"
                               value="Add ${label(childName)}"
                                view="/admin/${childEditPageName}.xhtml">
                            <f:param name="${childName}From" value="${entityName}"/>
                            <f:param name="openTab" value="${property.name}"/>
                        </s:button>
                    </div>
        </#if>
                </div>
            </div>
            </div>
        </div>
    </#if>
</#foreach>
    </div>
</div>
</ui:define>
    <ui:define name="script">
        <script src="${'#'}{request.contextPath}/static/js/bootbox.min.js"></script>
        <script src="${'#'}{request.contextPath}/static/js/bootbox.delete.min.js"></script>
    </ui:define>
</ui:composition>
