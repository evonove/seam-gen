<!DOCTYPE composition PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<#include "../util/TypeInfo.ftl">
<#assign entityName = pojo.shortName>
<#assign componentName = entityName?uncap_first>
<#assign homeName = componentName + "Home">
<#assign masterPageName = entityName + "List">
<#assign pageName = entityName>
<#assign datepicker = false>
<#assign timepicker = false>
<#assign oneToMany = false>
<#assign manyToMany = false>
<#assign sort="it.evonove.seam.ftl.FieldAnnotationSorter"?new()>

<ui:composition xmlns="http://www.w3.org/1999/xhtml"
    xmlns:s="http://jboss.org/schema/seam/taglib"
    xmlns:ui="http://java.sun.com/jsf/facelets"
    xmlns:f="http://java.sun.com/jsf/core"
    xmlns:h="http://java.sun.com/jsf/html"
    xmlns:a="http://richfaces.org/a4j"
    template="/layout/template.xhtml">

<ui:define name="body">
    <h:panelGroup rendered="${'#'}{(not empty facesContext.messageList) and (facesContext.validationFailed)}">
      <div class="alert alert-error fade in">
        <a class="close" data-dismiss="alert">×</a> Validation failed: check fields below
      </div>
    </h:panelGroup>
    <div class="row-fluid">
      <div class="span12">
        <div class="well">
          <ui:fragment rendered="${'#'}{${homeName}.managed}">
            <h2>${'#'}{msg['Edit']} ${'#'}{msg['${entityName}']}
              "${'#'}{${homeName}.instance}"</h2>
          </ui:fragment>
          <ui:fragment rendered="${'#'}{!${homeName}.managed}">
            <h2>${'#'}{msg['New']} ${'#'}{msg['${entityName}']}</h2>
          </ui:fragment>
        </div>
      </div>
    </div>
    <h:form class="form-horizontal">
      <div class="row-fluid">
        <div class="span12">
          <div class="well well-small form-inline form-no-margin">
            <h:commandButton styleClass="btn btn-success"
              value="${'#'}{msg['Save']}" action="${'#'}{${homeName}.persist}"
              disabled="${'#'}{!${homeName}.wired}"
              rendered="${'#'}{!${homeName}.managed}">&nbsp;
                        <s:conversationId />
            </h:commandButton>

            <h:commandButton styleClass="btn btn-success"
              value="${'#'}{msg['Update']}" action="${'#'}{${homeName}.update}"
              rendered="${'#'}{${homeName}.managed}">&nbsp;
                        <s:conversationId />
            </h:commandButton>

            <s:button styleClass="btn" value="${'#'}{msg['Cancel']}" rendered="${'#'}{!${homeName}.managed}"
              propagation="end" view="/admin/${'#'}{empty ${componentName}From ? '${masterPageName}' : ${componentName}From}.xhtml">
                <f:param name="openTab" value="${'#'}{openTab}"/>
            </s:button>

            <s:button styleClass="btn" value="${'#'}{msg['Back']}" rendered="${'#'}{${homeName}.managed}"
              propagation="end" view="/admin/${entityName}.xhtml">
                <f:param name="openTab" value="${'#'}{openTab}"/>
            </s:button>
          </div>
        </div>
      </div>
      <div class="row-fluid">
        <div class="span12">
          <div class="well">
          		<!-- pojo.getDecoratedObject.properties arraylist to sort but private -->
          		
              <#foreach property in sort(pojo.allPropertiesIterator)>
              	<#include "editproperty.xhtml.ftl">
              </#foreach>
          </div>
        </div>
      </div>
      <div class="row-fluid">
        <div class="span12">
          <div class="well well-small form-inline form-no-margin">
            <h:commandButton styleClass="btn btn-success"
              value="${'#'}{msg['Save']}" action="${'#'}{${homeName}.persist}"
              disabled="${'#'}{!${homeName}.wired}"
              rendered="${'#'}{!${homeName}.managed}">&nbsp;
                        <s:conversationId />
            </h:commandButton>

            <h:commandButton styleClass="btn btn-success"
              value="${'#'}{msg['Update']}" action="${'#'}{${homeName}.update}"
              rendered="${'#'}{${homeName}.managed}">&nbsp;
                        <s:conversationId />
            </h:commandButton>

            <s:button styleClass="btn" value="${'#'}{msg['Cancel']}" rendered="${'#'}{!${homeName}.managed}"
              propagation="end" view="/admin/${'#'}{empty ${componentName}From ? '${masterPageName}' : ${componentName}From}.xhtml">
                <f:param name="openTab" value="${'#'}{openTab}"/>
            </s:button>

            <s:button styleClass="btn" value="${'#'}{msg['Back']}" rendered="${'#'}{${homeName}.managed}"
              propagation="end" view="/admin/${entityName}.xhtml">
                <f:param name="openTab" value="${'#'}{openTab}"/>
            </s:button>
          </div>
        </div>
      </div>
    </h:form>
</ui:define>

<#if datepicker || timepicker || oneToMany || manyToMany>
  <ui:define name="css">
  <#if datepicker>
    <link rel="stylesheet" type="text/css" href="${'#'}{request.contextPath}/static/css/datepicker.css" />
  </#if>
  <#if timepicker>
    <link rel="stylesheet" type="text/css" href="${'#'}{request.contextPath}/static/css/bootstrap-timepicker.min.css" />
  </#if>
  <#if !chained?? && (oneToMany || manyToMany)>
    <link rel="stylesheet" type="text/css" href="${'#'}{request.contextPath}/static/css/select2.css" />
  </#if>
  </ui:define>

  <ui:define name="script">
  <#if datepicker>
      <script type="text/javascript" src="${'#'}{request.contextPath}/static/js/bootstrap-datepicker.js"></script>
      <script type="text/javascript" src="${'#'}{request.contextPath}/static/js/bootstrap-datepicker.it.js" charset="UTF-8"></script>
  </#if>
  <#if timepicker>
      <script type="text/javascript" src="${'#'}{request.contextPath}/static/js/bootstrap-timepicker.min.js"></script>
  </#if>
  <#if !chained?? && (oneToMany || manyToMany)>
      <script type="text/javascript" src="${'#'}{request.contextPath}/static/js/select2.min.js"></script>
  </#if>
      <script type="text/javascript">
          $(document).ready(function() {
            <#if datepicker>
              $('div[id*="-calendar"]').datepicker({'startView': 0, 'autoclose' : true, 'language' : 'en'});
            </#if>
            <#if timepicker>
              $('input[id*="-time"]').timepicker({ template: 'dropdown', showSeconds: true, showMeridian: false });
            </#if>
            <#if !chained?? && (oneToMany || manyToMany)>
              $('select').not('.chained').select2();
            </#if>
          });
      </script> 
  </ui:define>
</#if>
</ui:composition>
