<#include "../util/TypeInfo.ftl">

<#if !property.equals(pojo.identifierProperty) || property.value.identifierGeneratorStrategy == "assigned">
<#if c2j.isComponent(property)>
<#foreach componentProperty in property.value.propertyIterator>
        <s:decorate id="${componentProperty.name}" template="/layout/display.xhtml">
            <ui:define name="label">${label(componentProperty.name)}</ui:define>
            <@outputValue property=componentProperty expression="${'#'}{${homeName}.instance.${property.name}.${componentProperty.name}}" indent=12/>
        </s:decorate>
</#foreach>
<#else>
		<s:fragment rendered="${'#'}{not empty ${homeName}.instance.${property.name}}">
	        <dt>${'#'}{msg['${label(property.name)}']}:</dt>
	        <dd class="clearfix"><h:outputText value="${'#'}{${homeName}.instance.${property.name}}" /></dd>
	    </s:fragment>
</#if>
</#if>
