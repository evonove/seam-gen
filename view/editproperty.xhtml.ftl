<#include "../util/TypeInfo.ftl">
<#assign chainer="it.evonove.seam.ftl.FieldAnnotationChainer"?new()>
<#assign hasOrphanRemoval="it.evonove.seam.ftl.FieldAnnotationOrphanRemoval"?new()>


<#if property != pojo.versionProperty!>
	<#if !c2h.isCollection(property) && !isToOne(property)>
		<#assign propertyIsId = property.equals(pojo.identifierProperty)>
		<#if !propertyIsId || property.value.identifierGeneratorStrategy == "assigned">
			<#if pojo.isComponent(property)>
			
				<#foreach componentProperty in property.value.propertyIterator>
					<#assign column = componentProperty.columnIterator.next()>
            <s:decorate id="${componentProperty.name}Field" template="/layout/edit.xhtml">
                <ui:define name="label">${label(componentProperty.name)}</ui:define>
					<#if isDate(componentProperty)>
                <rich:calendar id="${componentProperty.name}"
						<#if propertyIsId>
                       disabled="${'#'}{${homeName}.managed}"
						</#if>
						<#if !column.nullable>
                       required="true"
						</#if>
                          value="${'#'}{${homeName}.instance.${property.name}.${componentProperty.name}}" datePattern="MM/dd/yyyy" />
					<#elseif isTime(componentProperty)>
                <h:inputText id="${componentProperty.name}"
                           size="5"
						<#if !column.nullable>
                       required="true"
						</#if>
                          value="${'#'}{${homeName}.instance.${property.name}.${componentProperty.name}}">
                    <f:convertDateTime type="time"/>
                    <a:ajax event="blur" render="${componentProperty.name}Field" bypassUpdates="true"/>
                </h:inputText>
					<#elseif isTimestamp(componentProperty)>
                <rich:calendar id="${componentProperty.name}"
						<#if !column.nullable>
                       required="true"
						</#if>
                          value="${'#'}{${homeName}.instance.${property.name}.${componentProperty.name}}" datePattern="MM/dd/yyyy hh:mm a" />
					<#elseif isBigDecimal(componentProperty)>
                <h:inputText id="${componentProperty.name}"
						<#if !column.nullable>
                       required="true"
						</#if>
                          value="${'#'}{${homeName}.instance.${property.name}.${componentProperty.name}}"
                           size="${column.precision+7}">
                    <a:ajax event="blur" render="${componentProperty.name}Field" bypassUpdates="true"/>
                </h:inputText>
					<#elseif isBigInteger(componentProperty)>
                <h:inputText id="${componentProperty.name}"
						<#if propertyIsId>
                       disabled="${'#'}{${homeName}.managed}"
						</#if>
						<#if !column.nullable>
                       required="true"
						</#if>
                          value="${'#'}{${homeName}.instance.${property.name}.${componentProperty.name}}"
                           size="${column.precision+6}">
                    <a:ajax event="blur" render="${componentProperty.name}Field" bypassUpdates="true"/>
                </h:inputText>
					<#elseif isBoolean(componentProperty)>
                 <h:selectBooleanCheckbox id="${componentProperty.name}"
						<#if !column.nullable>
                                    required="true"
						</#if>
						<#if propertyIsId>
                                    disabled="${'#'}{${homeName}.managed}"
						</#if>
                                       value="${'#'}{${homeName}.instance.${property.name}.${componentProperty.name}}"/>
					<#elseif isString(componentProperty)>
						<#if column.length gt 160>
							<#if column.length gt 800>
								<#assign rows = 10>
							<#else>
								<#assign rows = (column.length/80)?int>
							</#if>
                <h:inputTextarea id="${componentProperty.name}"
                               cols="80"
                               rows="${rows}"
							<#if propertyIsId>
                           disabled="${'#'}{${homeName}.managed}"
							</#if>
							<#if !column.nullable>
                           required="true"
							</#if>
                              value="${'#'}{${homeName}.instance.${property.name}.${componentProperty.name}}"/>
						<#else>
							<#if column.length gt 100>
								<#assign size = 100>
							<#else>
								<#assign size = column.length>
							</#if>
                <h:inputText id="${componentProperty.name}"
							<#if propertyIsId>
                       disabled="${'#'}{${homeName}.managed}"
							</#if>
							<#if !column.nullable>
                      required="true"
							</#if>
                          size="${size}"
                     maxlength="${column.length}"
                         value="${'#'}{${homeName}.instance.${property.name}.${componentProperty.name}}">
                    <a:ajax event="blur" render="${componentProperty.name}Field" bypassUpdates="true"/>
                </h:inputText>
						</#if>
					<#else>
                <h:inputText id="${componentProperty.name}"
						<#if !column.nullable>
                       required="true"
						</#if>
						<#if propertyIsId>
                       disabled="${'#'}{${homeName}.managed}"
						</#if>
                          value="${'#'}{${homeName}.instance.${property.name}.${componentProperty.name}}">
                    <a:ajax event="blur" render="${componentProperty.name}Field" bypassUpdates="true"/>
                </h:inputText>
					</#if>
            </s:decorate>
			</#foreach>

		<#else>

		<#assign column = property.columnIterator.next()>
		<#assign property = property.value.typeName>

<#if isDate(property)>
            <#assign datepicker = true>
            <div id="${property.name}-calendar" class="control-group input-append date ${'#'}{!${property.name}.valid ? 'error' : ''}" data-date-format="dd/mm/yyyy">
              <h:outputLabel for="${property.name}-calendar" value="${'#'}{msg['${label(property.name)}']}" styleClass="control-label" />
              <div class="controls">
                <h:inputText id="${property.name}-calendar" styleClass="span5" value="${'#'}{${homeName}.instance.${property.name}}" binding="${'#'}{${property.name}}" />
                <span class="add-on">
                  <i class="icon-calendar"></i>
                </span>
              </div>
            </div><br/>
<#elseif isTime(property)>
      <#assign timepicker = true>
	      <div class="control-group input-append ${'#'}{!${property.name}.valid ? 'error' : ''}">
	        <h:outputLabel for="${property.name}-time" value="${'#'}{msg['${label(property.name)}']}" styleClass="control-label" />
	        <div class="controls bootstrap-timepicker">
	                <h:inputText id="${property.name}-time"
	                <#if !column.nullable>
	                       required="true"
	                </#if>
	              type="text" value="${'#'}{${homeName}.instance.${property.name}}" binding="${'#'}{${property.name}}" styleClass="input-small">
	                  <f:convertDateTime pattern="HH:mm:ss" type="time"/>
	                </h:inputText>
	                <span class="add-on">
	                    <i class="icon-time"></i>
	                </span>
	            </div>
	      </div>
<#elseif isBoolean(property)>
		    <div class="control-group ${'#'}{!${property.name}.valid ? 'error' : ''}">
		      <h:outputLabel for="${property.name}" value="${'#'}{msg['${label(property.name)}']}" styleClass="control-label" />
		      <div class="controls">
		        <h:selectOneRadio id="${property.name}" 
		                  <#if !column.nullable>
		                    required="true"
		                  </#if>
		                       value="${'#'}{${homeName}.instance.${property.name}}" binding="${'#'}{${property.name}}">
		            <f:selectItem itemValue="false" itemLabel="False"  />
		            <f:selectItem itemValue="true" itemLabel="True" />
		        </h:selectOneRadio>
		      </div>
		    </div>
<#elseif isString(property)>
  <#if column.length gt 160>
    <#if column.length gt 800>
    <#assign rows = 10>
    <#else>
      <#assign rows = (column.length/80)?int>
	</#if>
            <div class="control-group ${'#'}{!${property.name}.valid ? 'error' : ''}">
              <h:outputLabel for="${property.name}" value="${'#'}{msg['${label(property.name)}']}" styleClass="control-label" />
              <div class="controls">
                <h:inputTextarea id="${property.name}" 
              <#if propertyIsId>
                   disabled="${'#'}{${homeName}.managed}"
              </#if>
              <#if !column.nullable>
                   required="true"
              </#if>
                     cols="80" rows="${rows}" value="${'#'}{${homeName}.instance.${property.name}}" binding="${'#'}{${property.name}}" />
                <h:message for="${property.name}" errorClass="help-inline" />
              </div>
            </div>
  <#else>
    <#if column.length gt 100>
      <#assign size = 100>
    <#else>
      <#assign size = column.length>
    </#if>
            <div class="control-group ${'#'}{!${property.name}.valid ? 'error' : ''}">
              <h:outputLabel for="${property.name}" value="${'#'}{msg['${label(property.name)}']}" styleClass="control-label" />
              <div class="controls">
                <h:inputText id="${property.name}" 
                  <#if propertyIsId>
                       disabled="${'#'}{${homeName}.managed}"
                  </#if>
                  <#if !column.nullable>
                       required="true"
                  </#if>
                          value="${'#'}{${homeName}.instance.${property.name}}" binding="${'#'}{${property.name}}" />
                <h:message for="${property.name}" errorClass="help-inline" />
              </div>
            </div>
  </#if>

<#elseif isDouble(property)>

            <div class="control-group ${'#'}{!${property.name}.valid ? 'error' : ''}">
              <h:outputLabel for="${property.name}" value="${'#'}{msg['${label(property.name)}']}" styleClass="control-label" />
              <div class="controls">
                <h:inputText id="${property.name}" 
                  <#if propertyIsId>
                       disabled="${'#'}{${homeName}.managed}"
                  </#if>
                  <#if !column.nullable>
                       required="true"
                  </#if>
                          value="${'#'}{${homeName}.instance.${property.name}}" binding="${'#'}{${property.name}}">
                          <f:convertNumber locale="it_IT" groupingUsed="false" pattern="0.00"/>
                </h:inputText>
                <h:message for="${property.name}" errorClass="help-inline" />
              </div>
            </div>

<#elseif isInteger(property)>

                <div class="control-group ${'#'}{!${property.name}.valid ? 'error' : ''}">
                  <h:outputLabel for="${property.name}" value="${'#'}{msg['${label(property.name)}']}" styleClass="control-label" />
                  <div class="controls">
                    <h:inputText id="${property.name}" 
                      <#if propertyIsId>
                           disabled="${'#'}{${homeName}.managed}"
                      </#if>
                      <#if !column.nullable>
                           required="true"
                      </#if>
                              value="${'#'}{${homeName}.instance.${property.name}}" binding="${'#'}{${property.name}}" />
                    <h:message for="${property.name}" errorClass="help-inline" />
                  </div>
                </div>
</#if>
</#if>
</#if>
<#else>

<div class="control-group ${'#'}{!${property.name}.valid ? 'error' : ''}">
    <#if isToOne(property)>
              <#assign oneToMany = true>
              <#assign parentPojo = c2j.getPOJOClass(cfg.getClassMapping(property.value.referencedEntityName))>
              <#assign parentName = parentPojo.shortName?uncap_first>
              <#assign parentList = parentName + "List">
              
              <h:outputLabel for="${property.name}" value="${'#'}{msg['${label(property.name)}']}" styleClass="control-label" />
              <div class="controls">
              <#assign chain=chainer(property)>
                      <#if chain.connected>
                      		<#global chained = true>
                			<h:selectOneMenu id="${property.name}" styleClass="chained" 
                              		 value="${'#'}{${homeName}.instance.${property.name}}" 
                              		 binding="${'#'}{${property.name}}">
                      		<#if chain.type = 'SLAVE' || chain.type = 'BOTH'>
                      			<s:selectItems value="${'#'}{${componentName}Action.get${chain.yourClassSimpleName?cap_first}Chained(${homeName}.instance.${chain.masterFieldName})}"
                      		<#else>
                      			<s:selectItems value="${'#'}{${parentList}.resultList}"
                      		</#if>
                                     var="_${parentName}"
                                     label="${'#'}{_${parentName}}"
                                     noSelectionLabel="Not available"
                                     hideNoSelectionLabel="false" />
                            <#if chain.type = 'MASTER' || chain.type = 'BOTH'>
                      			<a:ajax event="change" render="${chain.slaveFieldName}"/>
                      		</#if>
                      <#else>
                      		<h:selectOneMenu id="${property.name}" 
                              		 value="${'#'}{${homeName}.instance.${property.name}}" 
                              		 binding="${'#'}{${property.name}}">
                      		<s:selectItems value="${'#'}{${parentList}.resultList}"
                                     var="_${parentName}"
                                     label="${'#'}{_${parentName}}"
                                     noSelectionLabel="Not available"
                                     hideNoSelectionLabel="false" />
                      </#if>
                      <s:convertEntity /> 
                    </h:selectOneMenu>
                <h:message for="${property.name}" errorClass="help-inline" />
              </div>  

              
    <#elseif c2h.isOneToManyCollection(property)>
              <#assign manyToMany = true>
              <#assign childPojo = c2j.getPOJOClass(property.value.element.associatedClass)>
              <#assign childName = childPojo.shortName?uncap_first>
              <#assign childList = childName + "List">
			<h:outputLabel for="${property.name}" value="${'#'}{msg['${label(property.name)}']}" styleClass="control-label" />
              <div class="controls">
              <h:selectManyMenu id="${property.name}" binding="${'#'}{${property.name}}"
			  <#assign oneToManyBidirectional = !hasOrphanRemoval(property)>
			  <#if oneToManyBidirectional>
                collectionType="java.util.HashSet" value="${'#'}{${homeName}.instance.${property.name}}">
             <#else>
                collectionType="java.util.ArrayList" value="${'#'}{${homeName}.raw${childPojo.shortName}List}">
              </#if>
                 <s:selectItems value="${'#'}{${childList}.resultList}"
                                     var="_${childName}"
                                     label="${'#'}{_${childName}}"
                                     noSelectionLabel="Not available"
                                     hideNoSelectionLabel="false" />
               <s:convertEntity /> 
                    </h:selectManyMenu>
                <h:message for="${property.name}" errorClass="help-inline" />
              </div>
              
    <#else>
              <#assign manyToMany = true>
              <#assign typeName = c2j.getJavaTypeName(property, true)>
              <#assign type = typeName.substring(typeName.lastIndexOf('.') + 1,typeName.length()-1)>
              <#assign childName = type?uncap_first>
              <#assign childList = childName + "List">

              <h:outputLabel for="${property.name}" value="${'#'}{msg['${label(property.name)}']}" styleClass="control-label" />
              <div class="controls">
                <h:selectManyMenu id="${property.name}" binding="${'#'}{${property.name}}"
                collectionType="java.util.HashSet" value="${'#'}{${homeName}.instance.${property.name}}">
                      <s:selectItems value="${'#'}{${childList}.resultList}"
                                     var="_${childName}"
                                     label="${'#'}{_${childName}}"
                                     hideNoSelectionLabel="true" />
                      <s:convertEntity /> 
                    </h:selectManyMenu>
                <h:message for="${property.name}" errorClass="help-inline" />
              </div>
    </#if>
</div>
</#if>
</#if>
