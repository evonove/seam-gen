<!DOCTYPE composition PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<#include "../util/TypeInfo.ftl">
<#assign entityName = pojo.shortName>
<#assign componentName = entityName?uncap_first>
<#assign listName = componentName + "List">
<#assign pageName = entityName>
<#assign editPageName = entityName + "Edit">
<#assign listPageName = entityName + "List">
<ui:composition xmlns="http://www.w3.org/1999/xhtml"
    xmlns:s="http://jboss.org/schema/seam/taglib"
    xmlns:ui="http://java.sun.com/jsf/facelets"
    xmlns:f="http://java.sun.com/jsf/core"
    xmlns:h="http://java.sun.com/jsf/html"
    template="/layout/template.xhtml">

    <ui:define name="body">
        <div class="row-fluid">
            <div class="span12">
                <div class="well">
                    <h2>${'#'}{msg['List.of']} ${'#'}{msg['${label(componentName)}']}</h2>
                </div>
            </div>
        </div>
        <h:form>
            <div class="row-fluid">
                <div class="span12 well well-small">
                    <h4>${'#'}{msg['Search.Filter']}</h4>
                    <#assign searchParamNames = []/>
                    <#foreach property in pojo.allPropertiesIterator>
                    <#if !c2h.isCollection(property) && !isToOne(property) && property != pojo.versionProperty!>
                    <#if c2j.isComponent(property)>
                    <#foreach componentProperty in property.value.propertyIterator>
                    <#if isString(componentProperty)>
                    <#assign searchParamNames = searchParamNames + [componentProperty.name]/>
                    <h:inputText placeholder="${label(componentProperty.name)}" value="${'#'}{${listName}.${componentName}.${property.name}.${componentProperty.name}}"/>&nbsp;
                    </#if>
                    </#foreach>
                    <#else>
                    <#if isString(property)>
                    <#assign searchParamNames = searchParamNames + [property.name]/>
                    <h:inputText placeholder="${label(property.name)}" value="${'#'}{${listName}.${componentName}.${property.name}}"/>&nbsp;
                    </#if>
                    </#if>
                    </#if>
                    </#foreach>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12 well well-small">
                    <s:link styleClass="btn btn-success" view="/admin/${editPageName}.xhtml" includePageParams="false" propagation="none"><i class="icon-plus-sign icon-white" /> ${'#'}{msg['Add']} ${'#'}{msg['${label(componentName)}']}</s:link>
                    <span class="pull-right">
                        <s:button styleClass="btn" value="${'#'}{msg['Reset']}" includePageParams="false" />&nbsp;
                        <h:commandButton styleClass="btn btn-success" value="${'#'}{msg['Search']}" action="/admin/${listPageName}.xhtml" />
                    </span>
                </div>
            </div>
        </h:form>
		<h:form>
        <div class="row-fluid">
            <div class="span12">
                <div class="well">
                    <h:outputText value="The ${componentName} search returned no results" rendered="${'#'}{empty ${listName}.resultList}"/>
                    <h:dataTable value="${'#'}{${listName}.resultList}" binding="${'#'}{${listName}.htmlDataTable}" var="_${componentName}" styleClass="table table-hover" columnClasses="" rendered="${'#'}{not empty ${listName}.resultList}">
                        <h:column>
                            <s:link styleClass="btn btn-success" view="/admin/${editPageName}" propagation="none">
                                <f:param name="${componentName}Id" value="${'#'}{_${componentName}.id}"/>
                                <i class="icon-pencil icon-white" /> ${'#'}{msg['Edit']}
                            </s:link>
                        </h:column>
                        <#foreach property in pojo.allPropertiesIterator>
                        <#if !c2h.isCollection(property) && !isToOne(property) && property != pojo.versionProperty!>
                        <#if pojo.isComponent(property)>
                        <#foreach componentProperty in property.value.propertyIterator>
                        <h:column>
                            <f:facet name="header">
                                <#assign propertyPath = property.name + '.' + componentProperty.name>
                                <ui:include src="/layout/sort.xhtml">
                                    <ui:param name="entityList" value="${'#'}{${listName}}"/>
                                    <ui:param name="propertyLabel" value="${'#'}{msg['${label(componentProperty.name)}']}"/>
                                    <ui:param name="propertyPath" value="${componentName}.${propertyPath}"/>
                                </ui:include>
                            </f:facet>													
                            <@outputValue property=componentProperty expression="${'#'}{_${componentName}.${property.name}.${componentProperty.name}}" indent=12/>
                        </h:column>
                        </#foreach>
                        <#else>
                        <h:column>
                            <f:facet name="header">
                                <ui:include src="/layout/sort.xhtml">
                                    <ui:param name="entityList" value="${'#'}{${listName}}"/>
                                    <ui:param name="propertyLabel" value="${'#'}{msg['${label(property.name)}']}"/>
                                    <ui:param name="propertyPath" value="${componentName}.${property.name}"/>
                                </ui:include>
                            </f:facet>
                            <@outputValue property=property expression="${'#'}{_${componentName}.${property.name}}" indent=12/>
                        </h:column>
                        </#if>
                        </#if>
                        <#if isToOne(property)>
                        <#assign parentPojo = c2j.getPOJOClass(cfg.getClassMapping(property.value.referencedEntityName))>
                        <#if parentPojo.isComponent(parentPojo.identifierProperty)>
                        <#foreach componentProperty in parentPojo.identifierProperty.value.propertyIterator>
                        <h:column>
                            <f:facet name="header">
                                <#assign propertyPath = property.name + '.' + parentPojo.identifierProperty.name + '.' + componentProperty.name>
                                <ui:include src="/layout/sort.xhtml">
                                    <ui:param name="entityList" value="${'#'}{${listName}}"/>
                                    <ui:param name="propertyLabel" value="${'#'}{msg['${label(property.name)} ${label(componentProperty.name)?uncap_first}']}"/>
                                    <ui:param name="propertyPath" value="${componentName}.${propertyPath}"/>
                                </ui:include>
                            </f:facet>
                            <@outputValue property=componentProperty expression="${'#'}{_${componentName}.${propertyPath}}" indent=12/>
                        </h:column>
                        </#foreach>
                        <#else>
                        <h:column>
                            <f:facet name="header">
                                <#assign propertyPath = property.name + '.' + parentPojo.identifierProperty.name>
                                <ui:include src="/layout/sort.xhtml">
                                    <ui:param name="entityList" value="${'#'}{${listName}}"/>
                                    <ui:param name="propertyLabel" value="${'#'}{msg['${label(property.name)} ${label(parentPojo.identifierProperty.name)?uncap_first}']}"/>
                                    <ui:param name="propertyPath" value="${componentName}.${propertyPath}"/>
                                </ui:include>
                            </f:facet>
                            <@outputValue property=parentPojo.identifierProperty expression="${'#'}{_${componentName}.${propertyPath}}" indent=12/>
                        </h:column>
                        </#if>
                        </#if>
                        </#foreach>
                        <h:column>
                            <s:link styleClass="btn btn-info" view="/admin/${pageName}.xhtml">
                                <#if pojo.isComponent(pojo.identifierProperty)>
                                <#foreach componentProperty in pojo.identifierProperty.value.propertyIterator>
                                <f:param name="${componentName}${componentProperty.name?cap_first}" value="${'#'}{_${componentName}.${pojo.identifierProperty.name}.${componentProperty.name}}"/>
                                </#foreach>
                                <#else>
                                <f:param name="${componentName}${pojo.identifierProperty.name?cap_first}" value="${'#'}{_${componentName}.${pojo.identifierProperty.name}}"/>
                                </#if>
                                <i class="icon-eye-open icon-white" /> ${'#'}{msg['Open']}
                            </s:link>
                        </h:column>
                        <h:column>
                            <button type="button" class="btn btn-danger btn-confirm" onclick="confirm_deletion(${'#'}{${listName}.htmlDataTable.rowIndex});">
                                <i class="icon-trash icon-white" /> ${'#'}{msg['Delete']}
                            </button><h:commandLink styleClass="hide command-delete" action="${'#'}{${listName}.delete}" />
                        </h:column>
                    </h:dataTable>
                    <ui:include src="/layout/pagination.xhtml">  
                        <ui:param name="bean" value="${'#'}{listName}"/>
                    </ui:include>  
                </div>
            </div>
        </div>
        </h:form>
    </ui:define>

    <ui:define name="script">
        <script src="${'#'}{request.contextPath}/static/js/bootbox.min.js"></script>
        <script src="${'#'}{request.contextPath}/static/js/bootbox.delete.min.js"></script>
    </ui:define>
</ui:composition>
