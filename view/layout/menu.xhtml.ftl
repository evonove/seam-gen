<ui:composition
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:ui="http://java.sun.com/jsf/facelets"
    xmlns:h="http://java.sun.com/jsf/html"
    xmlns:f="http://java.sun.com/jsf/core"
    xmlns:s="http://jboss.org/schema/seam/taglib">

    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <s:link styleClass="brand" view="/home.xhtml" value="${'#'}{projectName}" propagation="none"/>
                <div class="nav-collapse collapse">
                    <ul class="nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">${'#'}{msg['Browse.your.entities']} <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <#foreach entity in c2j.getPOJOIterator(cfg.classMappings)>
                                    <li>
                                        <s:link view="/admin/${entity.shortName}List.xhtml"
                                            value="${'#'}{msg['${entity.shortName}']}"
                                            includePageParams="false"
                                            propagation="none"/>
                                    </li>
                                </#foreach>
                            </ul>
                        </li>
                    </ul>
                    <!-- @newMenuItem@ -->
                    <h:form styleClass="navbar-form pull-right">
            			<span class="color999 rightMarg5">${'#'}{msg['Language']} :</span>
						<h:selectOneMenu styleClass="span2 rightMarg5" value="${'#'}{language.localeCode}" onchange="submit()" valueChangeListener="${'#'}{language.countryLocaleCodeChanged}">
				   			<f:selectItems value="${'#'}{language.countriesInMap}" /> 
				   		</h:selectOneMenu>
                        <s:link styleClass="btn btn-success" view="/login.xhtml" value="${'#'}{msg['Login']}" rendered="${'#'}{not identity.loggedIn}" propagation="none"/>
                        <s:link styleClass="btn" view="/home.xhtml" action="${'#'}{identity.logout}" value="${'#'}{msg['Logout']}" rendered="${'#'}{identity.loggedIn}" propagation="none"/>
                    </h:form>
                </div>
            </div>
        </div>
    </div>
</ui:composition>
