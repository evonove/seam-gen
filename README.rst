Bootstrap seam-gen
==================

This is an enhancement of `seam-gen` tool used to produce a `Seam 2.3.x` application quickly.
The main goal is to fix some bugged feature from the original project with a replacement of Richfaces in favor of `Twitter Bootstrap <http://twitter.github.io/bootstrap/>`_ focusing on implementation of an admin CRUD project.

The basic idea is to write only your application models and then use this lightweight seam-gen to generate all your admin views.

Getting started
---------------

Before install and use this generator, follow the `Seam Framework <http://www.seamframework.org/Seam2/Seam2DistributionDownloads>`_ installation instructions. Then, on your `SEAM_HOME` folder, replace seam-gen with this repository.

You are able to create a project as usual but when you use `seam generate-ui` the new generator will be used.

Project status
==============

Actually the project is under heavy development stage.

Roadmap
-------

Highest priority to:

 * Add some helper Annotations for models to generate widget customization (like chained select)
 * Authentication for generated API
 * Generation of API methods tests with `Arquillian <http://arquillian.org/>`_
 * Widget fully JSF2 compliant

Changelog
---------

0.1.0 [2013-03-26]

 * Replacement of Richfaces widget with Twitter Bootstrap templates
